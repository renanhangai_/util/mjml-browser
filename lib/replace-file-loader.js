const fs = require( 'fs' );
const loaderUtils = require( 'loader-utils' );

module.exports = function( code ) {
	const options = loaderUtils.getOptions( this );
	if ( !options.file )
		return code;
	return fs.readFileSync( options.file, 'utf8' );
}