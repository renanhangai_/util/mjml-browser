const loaderUtils = require( 'loader-utils' );

module.exports = function( code ) {
	const options = loaderUtils.getOptions( this );
	if ( !options.module )
		return code;
	return `const _ = require( 'lodash' );
	module.exports = _.${options.module}`;
}