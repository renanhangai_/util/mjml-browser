# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="0.0.4"></a>
## [0.0.4](https://gitlab.com/renanhangai_/util/mjml-browser/compare/v0.0.3...v0.0.4) (2019-01-17)



<a name="0.0.3"></a>
## [0.0.3](https://gitlab.com/renanhangai_/util/mjml-browser/compare/v0.0.2...v0.0.3) (2019-01-17)



<a name="0.0.2"></a>
## [0.0.2](https://gitlab.com/renanhangai_/util/mjml-browser/compare/v0.0.1...v0.0.2) (2019-01-16)


### Bug Fixes

* Export of mjml ([1f20735](https://gitlab.com/renanhangai_/util/mjml-browser/commit/1f20735))



<a name="0.0.1"></a>
## 0.0.1 (2019-01-16)
