const path = require( "path" );
const webpack = require( "webpack" );

const IS_DEV = process.env.NODE_ENV !== 'production';
const { BundleAnalyzerPlugin } = require( "webpack-bundle-analyzer" );

module.exports = {
	mode: IS_DEV ? 'development' : 'production',
	devtool: 'inline-source-map',
	entry: './src/index.js',
	output: {
		path: path.resolve( __dirname, 'dist' ),
		filename: 'mjml-browser.js',
		library: 'mjml',
		libraryTarget: 'umd',
	},
	resolve: {
		alias: {
			'lodash/fp$': path.resolve( __dirname, 'src/mock/lodash/fp' ),
			'fs': path.resolve( __dirname, 'src/mock/fs' ),
			'crypto': path.resolve( __dirname, 'src/mock/crypto' ),
		},
	},
	plugins: [
		new BundleAnalyzerPlugin({ analyzerMode: 'static', openAnalyzer: false }),
		new webpack.NormalModuleReplacementPlugin( /^lodash/, function( resource ) {
			let request = resource.request;

			const isFp = request.match( /^lodash\/fp/ );
			if ( isFp )
				return;

			const lodashLoader = path.resolve( __dirname, 'lib/lodash-loader' );

			resource.request = resource.request
				.replace( /^lodash\.(\w+)/, replaceLodashDot )
				.replace( /^lodash\/(\w+)/, replaceLodashLoader );

			function replaceLodashDot( match, moduleName ) {
				const MODULES = {
					'assignin': 'assignIn',
					'foreach': 'forEach',
				};
				moduleName = MODULES[moduleName] || moduleName;
				return `lodash/${moduleName}`;
			}
			function replaceLodashLoader( match, moduleName ) {
				return `${lodashLoader}?module=${moduleName}!lodash/${moduleName}`;
			}
		}),
	],
	module: {
		rules: [
			{
				test:[/html-minifier/, /js-beautify/],
				use: 'null-loader',
			},
			mapFileReplaceRule({
				'./node_modules/mjml-core/lib/helpers/mjmlconfig.js': './src/mock/mjml/mjmlconfig.js',
				'./node_modules/mjml-core/lib/index.js': './src/mock/mjml/mjml-core.js',
				'./node_modules/mjml/lib/index.js': './src/mock/mjml/index.js',
			}),
		],
	},
	externals: {
		lodash : {
			commonjs: 'lodash',
			commonjs2: 'lodash',
			amd: 'lodash',
			root: '_'
		}
	},
};

function mapFileReplaceRule( files ) {
	const replaceFileLoader = path.resolve( __dirname, './lib/replace-file-loader' );

	const rules = [];
	for ( const originalFile in files ) {
		const replacedFile = files[originalFile];
		rules.push({
			test: path.resolve( __dirname, originalFile ),
			loader: replaceFileLoader,
			options: {
				file: path.resolve( __dirname, replacedFile ),
			},
		});
	}
	return { oneOf: rules };
}