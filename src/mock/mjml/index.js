'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mjmlCore = require('mjml-core');

var _mjmlCore2 = _interopRequireDefault(_mjmlCore);

var _mjmlValidator = require('mjml-validator');

var _mjmlSocial = require('mjml-social');

var _mjmlNavbar = require('mjml-navbar');

var _mjmlCarousel = require('mjml-carousel');

var _mjmlAccordion = require('mjml-accordion');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

registerComponent(require('mjml-body'), 'mj-body');
registerComponent(require('mjml-head'), 'mj-head');
registerComponent(require('mjml-head-attributes'), 'mj-attributes');
registerComponent(require('mjml-head-breakpoint'), 'mj-breakpoint');
registerComponent(require('mjml-head-font'), 'mj-font');
registerComponent(require('mjml-head-preview'), 'mj-preview');
registerComponent(require('mjml-head-style'), 'mj-style');
registerComponent(require('mjml-head-title'), 'mj-title');
registerComponent(require('mjml-hero'), 'mj-hero');
registerComponent(require('mjml-button'), 'mj-button');
registerComponent(require('mjml-column'), 'mj-column');
registerComponent(require('mjml-divider'), 'mj-divider');
registerComponent(require('mjml-group'), 'mj-group');
registerComponent(require('mjml-image'), 'mj-image');

registerComponent(require('mjml-raw'), 'mj-raw');
registerComponent(require('mjml-section'), 'mj-section');
registerComponent(require('mjml-spacer'), 'mj-spacer');
registerComponent(require('mjml-text'), 'mj-text');
registerComponent(require('mjml-table'), 'mj-table');
registerComponent(require('mjml-wrapper'), 'mj-wrapper');

registerComponent(_mjmlSocial.Social, 'mj-social');
registerComponent(_mjmlSocial.SocialElement, 'mj-social-element');
registerComponent(_mjmlNavbar.Navbar, 'mj-navbar');
registerComponent(_mjmlNavbar.NavbarLink, 'mj-navbar-link');
registerComponent(_mjmlAccordion.Accordion, 'mj-accordion');
registerComponent(_mjmlAccordion.AccordionElement, 'mj-accordion-element');
registerComponent(_mjmlAccordion.AccordionText, 'mj-accordion-text' );
registerComponent(_mjmlAccordion.AccordionTitle, 'mj-accordtion-title');
registerComponent(_mjmlCarousel.Carousel, 'mj-carousel');
registerComponent(_mjmlCarousel.CarouselImage, 'mj-carousel-image');

(0, _mjmlValidator.registerDependencies)(require('./dependencies'));

exports.default = _mjmlCore2.default;
module.exports = exports['default'];

function registerComponent( component, name ) {
	Object.defineProperty( component, "name", { value: name });
	(0, _mjmlCore.registerComponent)(component);
}