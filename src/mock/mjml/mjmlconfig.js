'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.registerCustomComponent = function () {
};
exports.resolveComponentPath = function () {
  return null;
};
exports.readMjmlConfig = function readMjmlConfig() {
  return { mjmlConfig: { packages: [] }, mjmlConfigPath: mjmlConfigPath, componentRootPath: null, error: new Error( 'Invalid component' ) }; 
};
exports.default = function() {
  return {
    success: [],
    failures: []
  };
};